#!/usr/bin/python3

import webapp

mensaje_ok = """
<!DOCTYPE html>
<html>
  <body>
    {content}
  </body>
</html>
"""

mensaje_not_found = """
<!DOCTYPE html>
<html>
  <body>
    <p>Recurso {resource} no disponible.</p>
  </body>
</html>
"""


class contentApp (webapp.webApp):
    """Simple web application for managing content."""

    content_dicc = {'/': 'Root page', '/page': 'A page', '/hola': 'Hola',
                    '/adios': 'Adios', '/prueba': 'Esto es una prueba'}

    def parse(self, request):
        """Return the resource name (including /)"""

        resource = request.split(' ', 2)[1]  # Trocea con espacio 2 veces y se queda el recurso (incluye /)
        print(resource)

        return resource

    def process(self, resource):
        """Process the relevant elements of the request."""

        if resource in self.content_dicc.keys():
            httpCode = "200 OK"
            content = self.content_dicc[resource]
            # Se muestra el contenido (el recurso se encuentra en el diccionario de contenidos)
            htmlBody = mensaje_ok.format(content=content)
        else:  # Si no se encuentra el recurso se devuelve un error.
            httpCode = "404 Not Found"
            htmlBody = mensaje_not_found.format(resource=resource)
        return (httpCode, htmlBody)


if __name__ == "__main__":
    miWebApp = contentApp("localhost", 1234)
